let digit1;
let digit2;
let sign;

do {
  digit1 = parseFloat(prompt("enter first digit"));
} while (isNaN(digit1) || digit1 === null || digit1 === undefined);

do {
  digit2 = parseFloat(prompt("enter second digit"));
} while (isNaN(digit2) || digit2 === null || digit2 === undefined);

do {
  sign = prompt("enter sign +, /, -, * ");
} while (sign !== "+" && sign !== "*" && sign !== "-" && sign !== "/");

console.log(digit1, digit2, sign);

const value = function (a, c, b) {
  let solve;
  if (c ==="+") {
    solve = a + b;
  } else if (c === "-") {
    solve = a - b;
  } else if (c === "*") {
    solve = a * b;
  } else if (c === "/") {
    solve = a / b;
  }
  return solve;
};

console.log(`result = ${value(digit1, sign, digit2)}`);

